var lokasi = {lat: -7.0378, lng: 111.0611};
var markers = new Array();
markers['kabupaten'] = new Array(); markers['masjid'] = new Array();
markers['provinsi'] = new Array(); markers['gereja'] = new Array();
markers['kecamatan'] = new Array(); markers['wihara'] = new Array();
var Lat = 0, Lng =0;
var map;

function initMap() {

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: lokasi,
    mapTypeId: 'roadmap'
  });
  infoWindow = new google.maps.InfoWindow;

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {

      lokasi = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      map.setCenter(lokasi);
      console.log("now");
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }

}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  console.log(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}


function ambil(get) {

  $("document").ready(function(){
    var data = {
      test: $( "#test" ).val()
    };
    var options = {
      url: base_url+"welcome/get/"+get,
      dataType: "json",
      type: "GET",
      data: { test: JSON.stringify( data ) }, // Our valid JSON string
      success: function( data, status, xhr ) {
        console.log(data['type']);
        var datas;
          for (var i = 0; i < data['message'].length; i++) {
            datas = data['message'][i];
            addMarker(data['type'], datas);
          }

      },
      error: function( xhr, status, error ) {
          console.log("huu "+error);
      }
    };
    $.ajax( options );
  });
}

function addMarker(icn, datas) {
  // console.log(datas);
    var contentString;
    var posisi ={lat: datas['latitude'], lng: datas['longitude']};
    posisi.lat = +posisi.lat; posisi.lng = +posisi.lng;

    switch (icn) {
      case 'masjid':
          icon = base_url+'assets/img/masjid.png';
          contentString = '';
      break;
      case 'gereja':
          icon = base_url+'assets/img/church.png';
          contentString = '';
      break;
      case 'wihara':
          icon = base_url+'assets/img/monastery.png';
          contentString = '';
      break;
      case 'kecamatan':
          icon = base_url+'assets/img/bo-kaap.png';
          var agama = new Array();
          for (var i = 0; i < datas["agama"].length; i++) {
            agama[i] = '<h3>'+datas["agama"][i]['nama_kelurahan']+'</h3>'+
            '&nbsp&nbsp&nbsp&nbsp Islam : '+datas["agama"][i]['islam']+'<br>'+
            '&nbsp&nbsp&nbsp&nbsp Kristen Protestan : '+datas["agama"][i]['kristen_protestan']+'<br>'+
            '&nbsp&nbsp&nbsp&nbsp Kristen Khatolik : '+datas["agama"][i]['kristen_katolik']+'<br>'+
            '&nbsp&nbsp&nbsp&nbsp Hindu : '+datas["agama"][i]['hindu']+'<br>'+
            '&nbsp&nbsp&nbsp&nbsp Budha : '+datas["agama"][i]['budha']+'<br>'+
            '&nbsp&nbsp&nbsp&nbsp Konghucu : '+datas["agama"][i]['konghucu']+'<br>'+
            '&nbsp&nbsp&nbsp&nbsp Budha : '+datas["agama"][i]['budha']+'<br>'+
            '&nbsp&nbsp&nbsp&nbsp Dai/Ustadz : '+datas["agama"][i]['jml_dai']+'<br>'+
            '&nbsp&nbsp&nbsp&nbsp Pesantren : '+datas["agama"][i]['pesantren']+'<br>';
            // datas["agama"][i]
          }
          contentString = '<div id="content">'+
                '<h1 id="firstHeading" class="firstHeading">MUI Kecamatan '+datas['nama_kecamatan']+'</h1>'+
                '<div id="siteNotice">'+
                '<p><b>'+datas["alamat"]+', </b> '+datas["email"]+' '+datas["telp"]+'</p>'+
                '</div>'+
                '<div id="bodyContent">'+
                  agama
                '</div>'+
                '</div>';
      break;
      case 'kabupaten':
          icon = base_url+'assets/img/kabupaten.png';
          contentString = '';
      break;
      default:

    }

    var infowindow = new google.maps.InfoWindow({
      content: contentString,
    });

    // var infoBubble = new InfoBubble();
    // infoBubble.addTab('');

  var marker = new google.maps.Marker({
    position: posisi,
    map: map,
    icon: icon
  });

  marker.addListener('click', function() {
    if (contentString != "") {
        infowindow.open(map, marker);
    }
    // if (contentString != "") {
    //     if (!infoBubble.isOpen()) {
    //         infoBubble.open(map, marker);
    //     }
    // }
  });

  markers[icn].push(marker);
}

function deleteMarkers(mark) {
    for (var i = 0; i < markers[mark].length; i++) {
        markers[mark][i].setMap(null);
    }

    markers[mark] = new Array();
}
$(document).ready(function () {
  $('input').on('click',function () {
      if ($('#kecamatan').is(':checked')) {
          ambil('kecamatan');
      } else {
          deleteMarkers('kecamatan');
      }

      if ($('#kabupaten').is(':checked')) {
          ambil('kabupaten');
      } else {
          deleteMarkers('kabupaten');
      }

      if ($('#provinsi').is(':checked')) {
          ambil('provinsi');
      } else {
          deleteMarkers('provinsi');
      }

      if ($('#masjid').is(':checked')) {
          ambil('masjid');
      } else {
          deleteMarkers('masjid');
      }

      if ($('#gereja').is(':checked')) {
          ambil('gereja');
      } else {
          deleteMarkers('gereja');
      }

      if ($('#wihara').is(':checked')) {
          ambil('wihara');
      } else {
          deleteMarkers('wihara');
      }
  });
});
