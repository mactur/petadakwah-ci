-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 16, 2019 at 05:32 PM
-- Server version: 10.2.19-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petn3697_petadakwah`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_keagamaan`
--

CREATE TABLE `data_keagamaan` (
  `id_agama` int(11) NOT NULL,
  `nama_kelurahan` varchar(100) NOT NULL,
  `jumlah_penduduk` int(11) NOT NULL,
  `islam` int(11) NOT NULL,
  `kristen_protestan` int(11) NOT NULL,
  `kristen_katolik` int(11) NOT NULL,
  `hindu` int(11) NOT NULL,
  `budha` int(11) NOT NULL,
  `konghucu` int(11) NOT NULL,
  `jml_masjid` int(11) NOT NULL,
  `pesantren` int(11) NOT NULL,
  `jml_dai` int(11) NOT NULL,
  `gereja` int(11) NOT NULL,
  `pura` int(11) NOT NULL,
  `kelenteng` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `tahun` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_keagamaan`
--

INSERT INTO `data_keagamaan` (`id_agama`, `nama_kelurahan`, `jumlah_penduduk`, `islam`, `kristen_protestan`, `kristen_katolik`, `hindu`, `budha`, `konghucu`, `jml_masjid`, `pesantren`, `jml_dai`, `gereja`, `pura`, `kelenteng`, `keterangan`, `tahun`, `id_kecamatan`, `updated_at`, `created_at`) VALUES
(1, 'Girirejoo', 1200, 1000, 50, 50, 40, 25, 35, 100, 80, 400, 8, 0, 0, 'mangapp', 2017, 1, '2018-11-20 13:42:34', NULL),
(2, 'Kebon Agung', 1000, 900, 50, 50, 0, 0, 0, 100, 3, 100, 0, 0, 0, 'good', 2018, 1, '2018-11-20 13:42:34', NULL),
(3, 'karang tengah', 100, 80, 80, 9, 7, 6, 0, 8, 5, 8, 7, 8, 8, 'baik', 2018, 1, '2018-11-20 15:51:36', '2018-11-19 17:00:00'),
(4, 'Imogiri', 150, 80, 8, 18, 8, 7, 30, 9, 90, 10, 2, 2, 1, 'mantabb', 2018, 2, '2018-11-20 15:52:35', '2018-11-20 17:00:00'),
(5, 'Karang talun', 80, 80, 0, 0, 0, 0, 0, 100, 90, 10, 0, 0, 0, 'hehe', 2018, 2, '2018-11-20 15:53:58', '2018-11-20 17:00:00'),
(6, 'Gatak', 200, 150, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'haha', 2018, 2, '2018-11-20 15:53:58', '2018-11-19 17:00:00'),
(7, 'Trirenggo', 400, 200, 0, 0, 0, 0, 0, 100, 100, 100, 0, 0, 0, '0', 2016, 2, '2018-11-25 21:04:57', '2018-11-25 21:04:57'),
(9, 'Pandak', 1000, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 'hai', 2018, 4, '2018-12-07 21:39:11', '2018-12-07 21:39:11'),
(10, 'Sriten', 1000, 12, 31, 38, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'baus', 2018, 2, '2018-12-07 21:44:02', '2018-12-07 21:44:02');

-- --------------------------------------------------------

--
-- Table structure for table `data_kecamatan`
--

CREATE TABLE `data_kecamatan` (
  `id_kec` int(11) NOT NULL,
  `nama_kecamatan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `telp` int(15) NOT NULL,
  `id_kab` int(11) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_kecamatan`
--

INSERT INTO `data_kecamatan` (`id_kec`, `nama_kecamatan`, `alamat`, `email`, `telp`, `id_kab`, `latitude`, `longitude`) VALUES
(1, 'Imogiri', 'bantul', 'imogiri@gmail.com', 98277272, 1, '-7.923', '110.390'),
(2, 'Sewon', 'bantul', 'sewon@gmail.com', 89771616, 1, '-7.846', '110.360');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id_dis` int(10) UNSIGNED NOT NULL,
  `kabupaten` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub` int(11) NOT NULL,
  `latitude` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id_dis`, `kabupaten`, `sub`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'Bantul', 1, '-7.840', '110.360', NULL, NULL),
(2, 'Kulon Progo', 1, '-7.7799', '110.1974', NULL, NULL),
(3, 'Gunung Kidul', 1, '-8.0023', '110.5738', NULL, NULL),
(4, 'Sleman', 1, '-7.7172', '110.4138', NULL, NULL),
(5, 'Kota Yogyakarta', 1, '-7.8009', '110.3826', NULL, NULL),
(6, 'Kota Jakarta Pusat', 3, '-6.17273432287489', '106.818298101425', '2018-12-02 12:38:41', '2018-12-02 12:38:41'),
(7, 'Kota Jakarta Utara', 3, '-6.149865', '106.896850', '2018-12-02 12:51:48', '2018-12-02 12:51:48'),
(8, 'Kota Jakarta Barat', 3, '-6.167327', '106.766188', '2018-12-02 12:52:59', '2018-12-02 12:52:59'),
(9, 'Kota Jakarta Selatan', 3, '-6.261570', '106.818215', '2018-12-02 12:55:02', '2018-12-02 12:55:02'),
(10, 'Kota Jakarta Timur', 3, '-6.227666', '106.895396', '2018-12-02 12:57:26', '2018-12-02 12:57:26');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `keterangan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'kecamatan', '2018-11-19 17:00:00', '2018-11-19 17:00:00'),
(2, 'kabupaten', '2018-11-19 17:00:00', '2018-11-19 17:00:00'),
(3, 'provinsi', '2018-11-19 17:00:00', '2018-11-19 17:00:00'),
(4, 'nasional', '2018-11-19 17:00:00', '2018-11-19 17:00:00'),
(5, 'admin', '2018-11-19 17:00:00', '2018-11-19 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_16_222643_add_telepon_to_users', 2),
(4, '2018_11_16_223909_create_levels_table', 3),
(5, '2018_11_16_224904_create_provinces_table', 3),
(6, '2018_11_16_224927_create_districts_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `organisasi_keagamaan`
--

CREATE TABLE `organisasi_keagamaan` (
  `id` int(11) NOT NULL,
  `nama_organisasi` varchar(100) NOT NULL,
  `pimpinan` varchar(100) NOT NULL,
  `lokasi` text NOT NULL,
  `jml_pengikut` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisasi_keagamaan`
--

INSERT INTO `organisasi_keagamaan` (`id`, `nama_organisasi`, `pimpinan`, `lokasi`, `jml_pengikut`, `keterangan`, `latitude`, `longitude`, `created_at`) VALUES
(1, 'Muhammadiyah', 'Ust Ghifari', 'gowok jogja', 100, 'good', '-7.678', '110.809', '2018-11-26 16:31:02'),
(2, 'Nahdatul Ulama', 'Ust Azmi', 'desaa', 200, 'good', '-7.908', '110.200', '2018-11-26 16:31:29'),
(3, 'GENERASI MUDA MATHLA\'UL ANWAR (GEMA MA) ', 'Ir. H. Andi Yudi Hendriawan, MRE', 'Jl. masjid Al-amanah Kavling POLRI No.1A Jelambar', 0, '- Menegakkan aqidah islam ditengah-tengah kehidupan bermasyarakat didalam NKRI yang berdasarkan Pancasila & UUD 1945, -Membina generasi muda yang Pancasilais, bertaqwa kepada allah SWT, cerdas, cakap, terampil, berjiwa patriotik, serta berkepribadian Indonesia.', '-6.156708', '106.779731', '2018-12-02 15:30:07'),
(4, 'HIMPUNAN MAHASISWA AL-WASHLIYAH (HIMMAH)', 'Aminullah Siagian', 'Komplek Cipinang indah Blog D9', 0, 'HIMMAH bertujuan untuk mewujudkan terbinanya mahasiswa sebagai kader islam yang memiliki kesadaran ilmiah, bertaqwa, berakhlak mulia dan bertanggung jawab kepada agama, bangsa dan negara.', '-6.230478', '106.891659', '2018-12-02 15:32:28'),
(5, 'PP SYARIKAT ISLAM', 'DR. Amrullah Ahmad,S.FIL.', 'Jl. Gatot Subroto Kav.72 Gd Prinkop AU Komplek Trikora', 0, '-Menjalankan Islam secara kaffah (Seluas dan sepenuh-penuhnya)\r\n-Membangun kebangsaan, mencari hak-hak kemanusiaan menjunjung derajat yang masih rendah.', '-6.2445264', '106.8407793', '2018-12-02 15:36:12'),
(6, 'ANGKATAN MUDA MAJELIS DAKWAH ISLAMIYAH (AM-MDI)', 'H. Ali Mochtar Ngabalin', 'Jl. Taman anggrek Nelly Slipi, Pal Merah', 0, 'AM-MDI bersifat organisasi kepemudaan islam yang terbuka serta berorientasi pada karya dan kekaryaan.', '-6.1854232', '106.7947377', '2018-12-02 15:37:48'),
(7, 'ANGKATAN MUDA ISLAM INDONESIA (AMII)', 'ANDI MUHAMMAD', 'JL.Jend. Basuki Rahmat No.8E', 0, 'AMII bersifat sebagai organisasi kemasyarakatan pemuda sebagai wadah berhimpun generasi muda islam yang memiliki persamaan dan kehendak.', '-6.228976', '106.883311', '2018-12-02 15:40:11'),
(8, 'ANGKATAN MUDA SATUAN KARYA ULAMA INDONESIA (AMSI)', 'Ade Akbar Ghadafi, SE', 'Jl. Rawasari Timur, Cempaka Putih', 0, 'AMSI bertujuan sebagai organisasi kader dan sebagai wadah berhimpunnya generasi muda Islam yang berorientasi pada kepemudaan.Amsi juga bertujuan : terciptanya generasi Islam pembangunan yang dijiwai oleh Pancasila dan UUD 1945', '-6.1833368', '106.8718166', '2018-12-02 15:41:17'),
(9, 'GENERASI MUDA PERSAUDARAAN MUSLIM INDONESIA (GM PARMUSI)', 'Ir. Ahmad Dolli Kurnia', 'Jalan Tebet Dalam IV E No.66, Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12810', 0, 'GM Parmusi bertujuan terbentuknya insan akademis, pencipta, pengabdi, yang bernafaskan islam yang diridhi Allah SWT.', '-6.2276523', '106.8513168', '2018-12-02 15:42:36'),
(10, 'KORP HMI-WATI (KOHATI)', 'Fitriyani Ismail', 'Jl. Diponegoro 16A, Menteng', 0, 'KOHATI berfungsi sebagai wadah peningkatan dan pengembangan potensi kader HMI dalam wacana dan dinamika keperempuan.', '-6.201107', '106.835516', '2018-12-02 15:45:37'),
(11, 'ANGKATAN PUTRI AL-WASHLIYAH (APA)', 'Siti Maryam, S.Ag', 'Jl. Howitzer Raya No.48 Sumur Batu, Kemayoran', 0, 'APA bertujuan mengamalkan ajaran islam untuk kebahagiaan dunia dan akhirat, mewijudkan kehidupan masyarakat yang islami, dan membina ukhwah islamiyah dengan para pemudi islam khususnya dalam umat islam pada umumnya.', '-6.162753', '106.867945', '2018-12-02 15:46:40'),
(12, 'GERAKAN PEMUDA ISLAM INDONESIA (GPII)', 'Ahmad Toha Almansur', 'Jl. Batu I No.2 (Gg.Arab) Pejaten, Pasar Minggu', 0, 'GPII berupaya untuk membentuk kader-kader yang berakhlak al-kharimah dan memiliki keislaman yang benar serta intelektualitas dan kreatifitas yang tinggi, sehingga seluruh kader GPII dapat memposisikan dirinya dalam kehidupan bermasyarakat dan bernegara sesuai dengan kepribadiannya sebagai seorang pemuda islam.', '-6.18735007483135', '106.82710647583', '2018-12-02 15:47:48'),
(13, 'KESATUAN AKSI MAHASISWA MUSLIM INDONESIA (KAMMI)', 'Muhammmad Ilyas', 'Jl. Penggalang Raya No.3, Matraman', 0, 'Wadah berjuang permanen yang akan melahirkan kader-kader pemimpin dalam upaya mewujudkan bangsa dan negara Indonesia yang islami.', '-6.20139745550722', '106.857554912567', '2018-12-02 15:48:55'),
(14, 'BADAN KOMUNIKASI PEMUDA REMAJA MASJID INDONESIA (BKPRMI)', 'H. Ali Mochtar Ngabalin', ' Jl. Taman Wijaya Kusuma Rt.08/02 MASJID ISTIQLAL KAMAR 13-14', 0, 'latar belakang berdirinya yaitu diantaranya : reaksi terhadap sebagai gejala social yang berkembang di tanah air, isu kebangkitan islam abad XV H, tumbuhnya kesadaran beragama dikalangan generasi muda islam.', '-6.17017', '106.83139', '2018-12-02 15:50:15'),
(15, 'AL-JAM\'IYATUL WASLIYAH', 'Prof.Dr.H.Muslim Nasution', 'Jl. Howhzer Raya No.46 Sumur Batu, Kemayoran', 0, 'latar belakang berdirinya yaitu ikut berperan serta mencapai kemerdekaan Indonesia secara berdaya guna dan berhasil guna dan sebagai wadah aspirasi umat islam dalam mengisi pembangunan Indonesia sekaligus pengejawentahan UUD 45 pasal 28', '-6.1623802', '106.8678674', '2018-12-02 15:51:27'),
(16, 'PEMUDA MUSLIM INDONESIA ', 'Dawak Faturohman', 'Jl. Taman amir Hamzah No.2, Menteng', 0, 'wadah berhimpun generasi muda islam dalam membela, mempertahankan dan mengisi kemerdekaan Negara Republik Indonesia.', '-6.204851', '106.849743', '2018-12-02 15:52:27'),
(17, 'GERAKAN MAHASISWA ISLAM INDONESIA (GMII)', 'Niko Kapisan', 'Jl. Proklamasi No.56 Lt.6 Gg. Perintis Kemerdekaan, Menteng', 0, 'Berfungsi sebagai organisasi kader, dan bertujuan terbentuknya mahasiswa islam yang berilmu, beriman dan memiliki semangat kebangsaan demi tercapainya Indonesia yang berkeadilan sosial.', '-6.203739', '106.8458239', '2018-12-02 15:53:35'),
(18, 'IKATAN MAHASISWA MUHAMMADIYAH (IMM)', 'Ton Abdillah Has', 'Jl. Menteng Raya No.62, Menteng', 0, 'Mengusahakan terbentuknya akademis islam yang berakhlak mulia dalam rangka mencapai tujuan Muhammadiyah.', '-6.1850043', '106.8349264', '2018-12-02 15:54:43'),
(19, 'DPP PENGAJIAN AL-HIDAYAH', 'Dra. Ny.Hj. Yani Marwan, Mpd.', 'Jl. anggrek Nelly Murni Slipi, Pal Merah', 0, 'Membangun manusia seutuhnya dalam tata susunan masyarakat pancasila yang adil dan makmur baik jasmani maupun rohani.', '-6.1854232', '106.7947377', '2018-12-02 15:56:33'),
(20, 'DEWAN PIMPINAN PUSAT LEMBAGA DAKWAH QASIDAH INDONESIA (DPP LASQI)', 'Drs.H. Ghazali Abbas Adan', 'Jl. HR. rasuna Said Kav.C 22 Kuningan, Setiabudi', 0, 'Melaksanakan dakwah islamiyah melalui seni qasidah dan meningkatkan rasa persaudaraan yang berpijak kepada semangat persatuan.', '-6.2231299', '106.8342426', '2018-12-02 15:57:48'),
(21, 'PEMUDA MUHAMMADIYAH', 'Dr.Saleh P. Daulay, M.Ag., M.Hum., MA.', 'Jl. Menteng Raya No.60 Gedung Dakwah Muhammadiyah', 0, 'Menghimpun, membina dan menggerakkan potensi pemuda islam serta meningkatkan perannya sebagai kader untuk mencapai tujuan Muhammadiyah.', '-6.184768', '106.834551', '2018-12-02 15:59:23'),
(22, 'GERAKAN PEMUDA AL-WASHLIYAH (GPA)', 'Wizdan Fauran Lubis ', 'jl. Kayu Manis IV Baru No.41, Matraman', 0, 'Mengamalkan ajaran islam untuk terwujudnya pemuda yang beriman, bertaqwa kepada allah SWT guna berperan aktif dalam pembangunan Nasional.', '-6.200988', '106.863363', '2018-12-02 16:00:39'),
(23, 'IKATAN PERSAUDARAAN HAJI INDONESIA', 'H. Try Sutrisno', 'Jl. Tegalan No 1, Matraman', 0, 'Memelihara dan mengupayakan pelestarian haji mabrur, guna meningkatkan partisipasi umat dalam pembangunan bangsa dan negara yang diridhoi Allah SWT', '-6.20217341039725', '106.857259869576', '2018-12-02 16:01:40'),
(24, 'PEMUDA ISLAM', 'Pemi Apriyanto', 'Jl. Rawamangun No.30, Cempaka Putih', 0, 'Meningkatkan dan memberdayakan potensi pemuda dalam berbagai aspek kehidupan bangsa, guna terwujudnya ketahanan bangsa dalam mensukseskan pembangunan nasional.', '-6.193909', '106.858013', '2018-12-02 16:02:38'),
(25, 'PIMPINAN PUSAT PERSATUAN WANITA TARBIYAH ISLAMIYAH (PERWATI)', 'Dra.Hj. Nilmayettin Yusri', 'Jl. Paseban No.11A, Menteng', 0, 'Memajukan kesejahteraan umum khususnya kaum wanita terutama dalam pendidikan baik formal maupun non formal serta mencerdaskan kehidupan bangsa dalam meningkatkan sumber daya manusia.', '-6.192761', '106.850011', '2018-12-02 16:03:49'),
(26, 'PERSATUAN UMAT ISLAM (PUI)', 'Drs. KH.E. Zaenal abidin', 'Jl. Cimandiri No.6 Falat 2 Lt.2, Menteng', 0, 'Mengintegrasikan pesantren dengan sistim Madrasah/sekolah, dimana para santri selain belajar disurau pada pagi/siang hari, belajar juga dikelas duduk dibangku menghadap meja dan papan tulis.', '-6.1930719', '106.8403969', '2018-12-02 16:04:44'),
(27, 'PP. Muhammadiyah', 'Prof.Dr.Muhammad Sirajuddin Syamsuddin, MA', 'Jl. Menteng Raya No.62', 0, 'Meningkatkan pengertian dan kematangan anggota Muhammadiyah tentang hak dan kewajiban sebagai warga negara dan meningkatkan kepekaan sosial terhadap persoalan-persoalan dan kesulitan hidup masyarakat.', '-6.18741940602558', '106.827707290649', '2018-12-02 16:05:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `problem_keagamaan`
--

CREATE TABLE `problem_keagamaan` (
  `id` int(11) NOT NULL,
  `id_kelurahan` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nama_problem` varchar(100) NOT NULL,
  `uraian_problem` text NOT NULL,
  `lokasi_problem` text NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `latitude` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `problem_keagamaan`
--

INSERT INTO `problem_keagamaan` (`id`, `id_kelurahan`, `tanggal`, `nama_problem`, `uraian_problem`, `lokasi_problem`, `longitude`, `latitude`) VALUES
(1, 1, '2018-11-20', 'perang salip', 'bacok caokan', 'gowok', '110.376', '-7.753'),
(2, 6, '2015-01-01', 'JEMAAT AHMADIYAH DENGAN FORUM UMAT ISLAM (FUI)', 'Ormas FUI Menganggap Aliran Ahmadiyah Adalah Sesat dan Telah Menodai Agama Islam (Tahun kejadian 2013)', 'MASJID AL-HIDAYAH JL.BALIKPAPAN I/10 GAMBIR', '-6.168694', '106.8123992'),
(3, 15, '2015-01-01', 'FPI, PTDII DENGAN POK AHMADIYAH', 'Penolakan Keberadaan Ahmadiyah, 05 Nop 2010 Jam 13:15 Wib PTDII Datangi Musholla Ahmadiyah dan Tuntut Ahmadiyah Dibubarkan.', 'MUSHOLA ALNURDIN KEBON BAWANG TANJUNG PRIOK', '-6.1211077', '106.8978299'),
(4, 17, '2015-01-01', 'GEREJA GPDI IMMANUEL SUKAPURA CLINCING', 'Rencana Pendirian Gereja Belum Ada Ijin Dari Pemerintah Daerah Ditolak Masyarakat, Penyetopan Penyegatan Kegiatan Agama. (Tahun Terjadi 2013)', 'RW.04 S/D 10 KEL.SUKAPURKOMP. A BEA CUKAI SUKAPURA', '-6.1124115', '106.9291504'),
(5, 28, '2015-01-01', 'AHMADIYAH', 'Adanya Kegiatan Ahmadiyah (Tahun Terjadinya 2013)', 'JL. BUKIT DURI TANJAKAN  GG BATU RT.02/08 KEL. BUKIT DURI, TEBET ', '-6.2218553', '106.8590096'),
(6, 31, '2015-01-01', 'Laskar Jihad dan FPI Menghajar Mahasiswa di Pengadilan', 'Bentrokan terjadi antara laskar Jihad Ahlusunnah dan Laskar FPI dengan mahasiswa pendukung terdakwa Mixilmina Munir di Pengadilan Negeri (PN) Jakarta Selatan Rabu (7/11) sekitar Pukul 12.00.Wib. Dua orang mahasiswa terluka akibat dikeroyok puluhan laskar. \r\nhttp://www.tempo.co/read/news/2001/11/07/05719628/Laskar-Jihad-dan-FPI-Menghajar-Mahasiswa-di-Pengadilan', 'Pengadilan Negeri Jakarta Selatan', '-6.28168004182142', '106.819885969162'),
(7, 32, '2015-01-01', 'Ormas FBR-PP', 'Bendera ormas diturunkan-saling ejek (Tahun terjadi 2013)', 'Jl. H Muhi Pondok Pinang/Tanah Kusir/LAP Garuda Cipulir/Jl. KH SyafiiHadzami Gandara', '-6.266376', '106.772219'),
(8, 36, '2015-01-01', 'FPI dan Jemaah Ahmadiyah', 'Ormas FPI tidak menyetujui dengan keberadaan masjid Ahmadiyah dikarenakan ajaran yang menyimpang.', 'Masjid Ahmadiyah JL. Moh Kahfi II RT 07/01', '-6.31122497767497', '106.813008785248'),
(9, 36, '2015-01-01', 'FPI DAN JEMAAH AHMADIYAH', 'Ormas FPU tidak menyetujui dengan keberadaan masjid ahmadiyah dikarenakan ajaran yang menyimpang (tahun terjadi 2013)', 'MASJID AHMADIYAH JL.MOH KAHFI II RT. 07/01 KEL.JGK', '-6.3118011', '106.813452'),
(10, 47, '2015-01-01', 'WARGA DENGAN PENDIRIAN GEREJA PAROKI ST MARIA VIANEY', 'Adanya pembangunan rumah ibadah umat khatolik, sedang adanya pembangunan gereja di lokasi tersebut. (tahun terjadi 2013)', 'JL. BAMBU WULUNG RT.005/03 KEL.SETU', '-6.3293718', '106.902553'),
(11, 41, '2015-01-01', 'WARGA SETEMPAT DENGAN PIHAT PENYELENGGARA IKSM THEOLOGI SANTOSA KASIH JAYA', 'Pendirian bangunan yayasan YAPI dengan ijin penampungan yatim piatu namun dikhawatirkan disalah gunakan sebagai tempat ibadah, 11 des 2012 penolakan warga terhadap penyalahgunaan ijin pembangunan yayasan sebagai tempat ibadah.', 'JL. RAYA CONDET NO.17 RT.5/3 KEL.BALEKAMBANG', '-6.2879768', '106.8727767'),
(12, 44, '2015-01-01', 'WARGA MASYARAKAT BUARAN II DENGAN PENGURUS GEREJA KRISTEN INDONESIA /GKI', 'Warga masyarakat tidak setuju akan direnovasi bangunan GKI dan agar ditinjau ulang  keberadaan GKI karena tidak memiliki IMB.', 'JL. BUARAN II RT. 004/013 KEL.KLENDER DUREN SAWIT', '-6.2182451', '106.917864'),
(13, 45, '2015-01-01', 'KAMPUS SEKOLAH THEOLO GI INJIL ARASTA MAR (STIA) DENGAN WARGA KAMPUNG PULO', 'Warga menolak keberadaan kampus STIA', 'KP. PULO RW.04/05 PINANG RANTI MAKASAR', '-6.291208', '106.886471'),
(14, 46, '2015-01-01', 'YAYASAN ADVENT INDONESIA DENGAN WARGA', 'Pendirian bangunan yayasan YAPI dengan ijin penampungan yatim piatu namun dikhawatirkan disalah gunkan sebagai tempat ibadah', 'JL. RAMITOR RT.07/08 RW.14 KEL CIBUBUR', '-6.3711055', '106.8860802'),
(15, 2, '2018-12-15', 'Jual Beli', 'Motor', 'UIN S', '-7.570739710957984', '110.59495710069575');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id_pro` int(10) UNSIGNED NOT NULL,
  `provinsi` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id_pro`, `provinsi`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'Yogyakarta', '-7.8272', '110.3556', '2018-11-19 17:00:00', '2018-11-19 17:00:00'),
(2, 'Jawa Tengah\r\n', '-7.1470', '110.0418', '2018-11-19 17:00:00', '2018-11-19 17:00:00'),
(3, 'DKI. Jakarta', '', '', '2018-12-02 13:47:03', '2018-12-02 13:47:03');

-- --------------------------------------------------------

--
-- Table structure for table `sub-district`
--

CREATE TABLE `sub-district` (
  `id_sub` int(11) NOT NULL,
  `kecamatan` varchar(100) NOT NULL,
  `sub` int(11) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub-district`
--

INSERT INTO `sub-district` (`id_sub`, `kecamatan`, `sub`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 'Imogiri', 1, '-7.917147', '110.382384', '2018-11-26 06:49:27', '2018-11-26 06:49:27'),
(2, 'Jetis', 1, '-7.910791', '110.363101', '2018-11-26 06:49:27', '2018-11-26 06:49:27'),
(3, 'Kokap', 2, '-7.829466', '110.095314', '2018-11-26 06:49:27', '2018-11-26 06:49:27'),
(4, 'Samigaluh', 2, '-7.679939', '110.168839', '2018-11-26 06:49:27', '2018-11-26 06:49:27'),
(5, 'Pengasih', 2, '-7.822932', '110.150042', '2018-11-26 06:49:27', '2018-11-26 06:49:27'),
(6, 'Gambir', 6, '-6.1727399', '106.8198066', '2018-12-02 14:08:22', '2018-12-02 14:08:22'),
(7, 'Sawah Besar', 6, '-6.1680843', '106.8316493', '2018-12-02 14:12:03', '2018-12-02 14:12:03'),
(8, 'Kemayoran', 6, '-6.1568711', '106.8648375', '2018-12-02 14:14:54', '2018-12-02 14:14:54'),
(9, 'Senen', 6, '-6.1848434793937', '106.84583902359', '2018-12-02 14:15:33', '2018-12-02 14:15:33'),
(10, 'Cempaka Putih', 6, '-6.1849395', '106.8716175', '2018-12-02 14:16:05', '2018-12-02 14:16:05'),
(11, 'Menteng', 6, '-6.18650210025836', '106.838339567184', '2018-12-02 14:16:54', '2018-12-02 14:16:54'),
(12, 'Tanah Abang', 6, '-6.1998478', '106.8146468', '2018-12-02 14:17:28', '2018-12-02 14:17:28'),
(13, 'Johar Baru', 6, '-6.18246486932994', '106.85632109642', '2018-12-02 14:18:12', '2018-12-02 14:18:12'),
(14, 'Penjaringan', 7, '-6.127133', '106.803433', '2018-12-02 14:18:43', '2018-12-02 14:18:43'),
(15, 'Tanjung Priok', 7, '-6.1522971', '106.8883204', '2018-12-02 14:20:51', '2018-12-02 14:20:51'),
(16, 'Koja', 7, '-6.1160336', '106.910987', '2018-12-02 14:21:18', '2018-12-02 14:21:18'),
(17, 'Cilincing', 7, '-6.10215', '106.93868', '2018-12-02 14:24:28', '2018-12-02 14:24:28'),
(18, 'Pademangan', 7, '-6.1317719', '106.8410358', '2018-12-02 14:24:57', '2018-12-02 14:24:57'),
(19, 'Kelapa Gading', 7, '-6.1571283', '106.8999654', '2018-12-02 14:25:23', '2018-12-02 14:25:23'),
(20, 'Cengkareng', 8, '-6.1290452', '106.7382008', '2018-12-02 14:25:56', '2018-12-02 14:25:56'),
(21, 'Grogol Petamburan', 8, '-6.1497697', '106.7817343', '2018-12-02 14:26:43', '2018-12-02 14:26:43'),
(22, 'Taman Sari', 8, '-6.134106', '106.8149023', '2018-12-02 14:27:16', '2018-12-02 14:27:16'),
(23, 'Tambora', 8, '-6.142366', '106.79901', '2018-12-02 14:27:53', '2018-12-02 14:27:53'),
(24, 'Kebon Jeruk', 8, '-6.2028025', '106.781616', '2018-12-02 14:29:30', '2018-12-02 14:29:30'),
(25, 'Kalideres', 8, '-6.1422848', '106.7030937', '2018-12-02 14:30:07', '2018-12-02 14:30:07'),
(26, 'Pal Merah', 8, '-6.204228', '106.79274', '2018-12-02 14:30:38', '2018-12-02 14:30:38'),
(27, 'Kembangan', 8, '-6.1986144', '106.7314614', '2018-12-02 14:31:09', '2018-12-02 14:31:09'),
(28, 'Tebet', 9, '-6.2357892', '106.8441721', '2018-12-02 14:34:56', '2018-12-02 14:34:56'),
(29, 'Setabudi', 9, '-6.208293', '106.82449', '2018-12-02 14:37:36', '2018-12-02 14:37:36'),
(30, 'Mampang Prapatan', 9, '-6.2515639', '106.8253946', '2018-12-02 14:38:01', '2018-12-02 14:38:01'),
(31, 'Pasar Minggu', 9, '-6.285806', '106.835461', '2018-12-02 14:38:57', '2018-12-02 14:38:57'),
(32, 'Kebayoran Lama', 9, '-6.2416349', '106.781231', '2018-12-02 14:39:36', '2018-12-02 14:39:36'),
(33, 'Cilandak', 9, '-6.289311', '106.790158', '2018-12-02 14:40:04', '2018-12-02 14:40:04'),
(34, 'Kebayoran Baru', 9, '-6.2379471', '106.7882455', '2018-12-02 14:41:59', '2018-12-02 14:41:59'),
(35, 'Pancoran', 9, '-6.250989', '106.857848', '2018-12-02 14:42:45', '2018-12-02 14:42:45'),
(36, 'Jagakarsa', 9, '-6.334051', '106.821708', '2018-12-02 14:43:30', '2018-12-02 14:43:30'),
(37, 'Pesanggrahan', 9, '-6.2388777', '106.7552218', '2018-12-02 14:44:00', '2018-12-02 14:44:00'),
(38, 'Matraman', 10, '-6.1971889', '106.8698247', '2018-12-02 14:44:34', '2018-12-02 14:44:34'),
(39, 'Pulogadung', 10, '-6.2029617', '106.9027084', '2018-12-02 14:45:04', '2018-12-02 14:45:04'),
(40, 'Jatinegara', 10, '-6.3580055', '106.9097325', '2018-12-02 14:45:35', '2018-12-02 14:45:35'),
(41, 'Kramatjati', 10, '-6.208763', '106.845599', '2018-12-02 14:46:08', '2018-12-02 14:46:08'),
(42, 'Pasar Rebo', 10, '-6.343441', '106.869055', '2018-12-02 14:46:52', '2018-12-02 14:46:52'),
(43, 'Cakung', 10, '-6.188953', '106.902941', '2018-12-02 14:48:26', '2018-12-02 14:48:26'),
(44, 'Duren Sawit', 10, '-6.2318867', '106.9179965', '2018-12-02 14:49:53', '2018-12-02 14:49:53'),
(45, 'Makasar', 10, '-6.279205', '106.878323', '2018-12-02 14:50:21', '2018-12-02 14:50:21'),
(46, 'Ciracas', 10, '-6.322449', '106.88409', '2018-12-02 14:50:43', '2018-12-02 14:50:43'),
(47, 'Cipayung', 10, '-6.315591', '106.893422', '2018-12-02 14:51:04', '2018-12-02 14:51:04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `telepon` int(11) NOT NULL,
  `id_daerah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rekening` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `isAdmin`, `remember_token`, `created_at`, `updated_at`, `telepon`, `id_daerah`, `bank`, `rekening`) VALUES
(1, 'admin', 'petadakwah@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 4, 'WuZPTfBIKuYfErTEYsyocyPv6H1saLc9NaQ8P0AUWHa2DbkbLVHS9bqC4j7I', '2018-11-16 10:02:21', '2018-11-16 10:02:21', 0, '2', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_keagamaan`
--
ALTER TABLE `data_keagamaan`
  ADD PRIMARY KEY (`id_agama`);

--
-- Indexes for table `data_kecamatan`
--
ALTER TABLE `data_kecamatan`
  ADD PRIMARY KEY (`id_kec`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id_dis`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisasi_keagamaan`
--
ALTER TABLE `organisasi_keagamaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `problem_keagamaan`
--
ALTER TABLE `problem_keagamaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id_pro`);

--
-- Indexes for table `sub-district`
--
ALTER TABLE `sub-district`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_keagamaan`
--
ALTER TABLE `data_keagamaan`
  MODIFY `id_agama` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `data_kecamatan`
--
ALTER TABLE `data_kecamatan`
  MODIFY `id_kec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id_dis` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `organisasi_keagamaan`
--
ALTER TABLE `organisasi_keagamaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `problem_keagamaan`
--
ALTER TABLE `problem_keagamaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id_pro` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub-district`
--
ALTER TABLE `sub-district`
  MODIFY `id_sub` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
