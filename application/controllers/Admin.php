<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Admin_md');
		$this->load->model('Login_md');

		if($this->session->userdata('masuk') != TRUE){
			$url=base_url();
			redirect($url);
		}
	}

	public function index() {
		echo "mode admin";
	}

	public function data() {

		$data['data'] = $this->Admin_md->get_keagamaan();
		$data['page'] = 'data';
		$this->load->view('admin/master');
		$this->load->view('admin/data',$data);
		$this->load->view('admin/footer');
	}
	public function problem() {
		$data['data'] = $this->Admin_md->get_problem("problem_keagamaan");
		$data['page'] = 'data';
		$this->load->view('admin/master');
		$this->load->view('admin/problem',$data);
		$this->load->view('admin/footer');
	}
	public function organisasi() {
		$data['data'] = $this->Admin_md->get_organisasi("organisasi_keagamaan");
		$data['page'] = 'data';
		$this->load->view('admin/master');
		$this->load->view('admin/organisasi',$data);
		$this->load->view('admin/footer');
	}
	public function tdata() {
		$this->load->view('admin/master');
		$this->load->view('admin/tdata');
		$this->load->view('admin/footer');
	}
	public function tproblem() {
		$this->load->view('admin/master');
		$this->load->view('admin/tproblem');
		$this->load->view('admin/footer');
	}
	public function torganisasi() {
		$this->load->view('admin/master');
		$this->load->view('admin/torganisasi');
		$this->load->view('admin/footer');
	}
	public function edata($id) {
		$data['keagamaan'] = $this->Admin_md->get_edit_keagamaan($id);
		$this->load->view('admin/master');
		$this->load->view('admin/edata', $data);
		$this->load->view('admin/footer');
	}
	public function eproblem($id) {
		$data['problem'] = $this->Admin_md->get_edit_problem($id);
		$this->load->view('admin/master');
		$this->load->view('admin/eproblem', $data);
		$this->load->view('admin/footer');
	}
	public function p_tdata(){
		if($this->Admin_md->m_tdata() == true){
			echo "
			<script>
			alert('Data Berhasil Ditambahkan');
			window.location.href = '" . base_url() . "admin/data/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Menambahkan');

				history.go(-1);

			</script>
			<?php
		}
	}
	public function p_edata(){
		if($this->Admin_md->m_edata() == true){
			echo "
			<script>
			alert('Data Berhasil Diupdate');
			window.location.href = '" . base_url() . "admin/data/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Mengupdate');

				history.go(-1);

			</script>
			<?php
		}
	}
	public function p_hdata($id){
		if($this->Admin_md->m_hdata($id) == true){
			echo "
			<script>
			alert('Data Berhasil Dihapus');
			window.location.href = '" . base_url() . "admin/data/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Menghapus');

				history.go(-1);

			</script>
			<?php
		}
	}
		public function p_tproblem(){
		if($this->Admin_md->m_tproblem() == true){
			echo "
			<script>
			alert('Data Berhasil Ditambahkan');
			window.location.href = '" . base_url() . "admin/problem/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Menambahkan');

				history.go(-1);

			</script>
			<?php
		}
	}
	public function p_eproblem(){
		if($this->Admin_md->m_eproblem() == true){
			echo "
			<script>
			alert('Data Berhasil Diupdate');
			window.location.href = '" . base_url() . "admin/problem/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Mengupdate');

				history.go(-1);

			</script>
			<?php
		}
	}
	public function p_hproblem($id){
		if($this->Admin_md->m_hproblem($id) == true){
			echo "
			<script>
			alert('Data Berhasil Dihapus');
			window.location.href = '" . base_url() . "admin/problem/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Menghapus');

				history.go(-1);

			</script>
			<?php
		}
	}
	public function p_torganisasi(){
		if($this->Admin_md->m_torganisasi() == true){
			echo "
			<script>
			alert('Data Berhasil Ditambahkan');
			window.location.href = '" . base_url() . "admin/organisasi/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Menambahkan');

				history.go(-1);

			</script>
			<?php
		}
	}
	public function p_eorganisasi(){
		if($this->Admin_md->m_eorganisasi() == true){
			echo "
			<script>
			alert('Data Berhasil Diupdate');
			window.location.href = '" . base_url() . "admin/organisasi/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Mengupdate');

				history.go(-1);

			</script>
			<?php
		}
	}
	public function p_horganisasi($id){
		if($this->Admin_md->m_horganisasi($id) == true){
			echo "
			<script>
			alert('Data Berhasil Dihapus');
			window.location.href = '" . base_url() . "admin/organisasi/';
			</script>
			";

		}else{
			?>
			<script>

				alert('Gagal Menghapus');

				history.go(-1);

			</script>
			<?php
		}
	}
}
