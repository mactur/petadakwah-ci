<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->model('Login_md');

			// if($this->session->userdata('masuk') == TRUE){
      //     $url=base_url();
      //     redirect(base_url().'admin');
      // }
	}

	public function index() {

	}

	public function login() {
			$this->load->view('auth/login');
	}

	public function cek_login() {
			$email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
			$password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

			$cek_user=$this->Login_md->auth_user($email,$password);

			if ($cek_user->num_rows() > 0) {

				$data=$cek_user->result_array();
				$this->session->set_userdata('masuk',TRUE);
				switch ($data[0]['isAdmin']) {
					case 1:
						$this->session->set_userdata('akses','1');
						$kdDaerah = 'kecamatan';
						$tbl = 'sub-district';
					break;
					case 2:
							$this->session->set_userdata('akses','2');
							$kdDaerah = 'kabupaten';
							$tbl = 'districts';
					break;
					case 3:
							$this->session->set_userdata('akses','3');
							$kdDaerah = 'provinsi';
							$tbl = 'provinces';
					break;
					case 4:
							$this->session->set_userdata('akses','4');
							$kdDaerah = 'nasional';
					break;
					default:
							$this->session->set_userdata('akses','0');
						break;
				}
				$this->session->set_userdata('ses_id',$data[0]['id']);
				$this->session->set_userdata('ses_nama',$data[0]['name']);
				$this->session->set_userdata('ses_idDaerah',$data[0]['id_daerah']);
				$this->session->set_userdata('ses_kdDaerah', $kdDaerah);
				redirect(base_url().'admin/data');
			} else {
				echo "string";
			}
		}

  public function logout(){
		$this->session->sess_destroy();
		redirect(base_url(''));
	}
}
