<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
			parent::__construct();
			$this->load->model('Maps_md');
	}

	public function get_prov($get="") {
			$data = $this->Maps_md->select_prov($get);
			echo json_encode($data);
	}

	public function get_kab($get="") {
			$data = $this->Maps_md->select_kab($get);
			echo json_encode($data);
	}

	public function get_kec($get="") {
			$data = $this->Maps_md->select_kec($get);
			echo json_encode($data);
	}

	public function set_kab($set="") {
			$data = $this->Maps_md->select_kab("",$set);
			$kabupaten = "<option value=''></option>";
			foreach ($data as $da ){
					$kabupaten.= '<option value="'.$da["id_dis"].'">'.$da["kabupaten"].'</option>';
			}
			echo $kabupaten;
			// echo json_encode($data);
	}

	public function set_kec($set="") {
			$data = $this->Maps_md->select_kec("",$set);
			$kecamatan = "<option value=''></option>";
			foreach ($data as $da ){
					$kecamatan.= '<option value="'.$da["id_kec"].'">'.$da["nama_kecamatan"].'</option>';
			}
			echo $kecamatan;
	}

	public function index() {
			$data['provinsi'] = $this->Maps_md->select_prov("");
			$data['kabupaten'] = $this->Maps_md->select_kab("");
			$data['kecamatan'] = $this->Maps_md->select_kec("");
			$this->load->view('layouts/master', $data);
	}

	public function get($get="") {
			switch ($get) {
					case 'kecamatan':
							$kcm = $this->Maps_md->get_kecamatan();
							for ($i=0; $i < count($kcm); $i++) {
								$agama = $this->Maps_md->get_keagamaan($kcm[$i]['id_kec']);
								$data[$i] = array(
									'nama_kecamatan'=> $kcm[$i]['nama_kecamatan'],
									'alamat'				=> $kcm[$i]['alamat'],
									'email'					=> $kcm[$i]['email'],
									'telp'					=> $kcm[$i]['telp'],
									'agama'					=> $agama,
									'latitude'			=> $kcm[$i]['latitude'],
									'longitude'			=> $kcm[$i]['longitude']
								);
							}
							// foreach ($kecamatan as $kcm) {
							// 		$agama = $this->Maps_md->get_keagamaan($kcm['id']);
							// 		$data = array(
							// 			'nama_kecamatan'=> $kcm['nama_kecamatan'],
							// 			'alamat'				=> $kcm['alamat'],
							// 			'email'					=> $kcm['email'],
							// 			'telp'					=> $kcm['telp'],
							// 			'agama'					=> $agama,
							// 			'latitude'			=> $kcm['latitude'],
							// 			'longitude'			=> $kcm['longitude']
							// 		);
							// }

						break;
					case 'kabupaten':
							$data = $this->Maps_md->get_kabupaten();
						break;
					case 'masjid':
							$data = array(
									['nama' => 'Masjid Jami Al-Inayah', 'latitude'=>'-7.7799', 'longitude'=>'110.3837'],
									['nama' => 'Masjid', 'latitude'=>'-7.7811', 'longitude'=>'110.3692'],
									['nama' => 'Masjid Jogokaryan', 'latitude'=>'-7.8238', 'longitude'=>'110.3644']
							);

						break;
					case 'gereja':
							$data = array(
									['nama' => 'Gereja Santa Mar', 'latitude'=>'-7.7819', 'longitude'=>'110.3646'],
									['nama' => 'Gereja Penyebaran Injil', 'latitude'=>'-7.8117', 'longitude'=>'110.3433'],
									['nama' => 'Gereja Kristen Jawa', 'latitude'=>'-7.809428', 'longitude'=>'110.351207']
							);
						break;
					case 'wihara':
							$data = array(
									['nama' => 'Tjen Ling Kiong Vihara', 'latitude'=>'-7.781187', 'longitude'=>'110.365636'],
									['nama' => 'Vihara Buddha Prabha', 'latitude'=>'-7.801261', 'longitude'=>'110.369729'],
									['nama' => 'Vihara Bodhicitta Maitreya', 'latitude'=>'-7.792034', 'longitude'=>'110.360115']
							);
						break;
				default:
					// code..
					break;
			}
			$datas = array('type' => $get, 'message' => $data);
			echo json_encode($datas);
	}
}
