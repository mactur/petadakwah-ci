<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="">

    <title>Peta Dakwah MUI</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link href="<?= base_url() ?>assets/css/style5.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/bootstrap-select.css" rel="stylesheet">
    <!-- <link rel="stylesheet" href="style5.css">
    <link rel="stylesheet" href="bootstrap-select.css"> -->
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <style>
      .btn {
        border-radius: 0
      }
      #map {
          height: 100%;
      }
    </style>
</head>

<body>

    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Peta Dakwah</h3>
            </div>

            <ul class="list-unstyled components">
                <!-- <p>Cariii</p> -->
                <li>
                  <span>Provinsi</span>
                  <div class="row-fluid" id="text_prov">
                    <select class="selectpicker" id="select_prov" data-live-search="true" title="Data Provinsi">
                        <?php foreach ($provinsi as $prov): ?>
                            <option value="<?= $prov['id_pro'] ?>"><?= $prov['provinsi'] ?></option>
                        <?php endforeach; ?>
                    </select>
                  </div>
                </li>
                <li>
                  <span>Kabupaten</span>
                  <div class=""  id="text_kab">
                    <select class="form-control" style="width:100%; border-radius:0" id="select_kab" data-live-search="true" title="Data Provinsi">

                    </select>
                  </div>
                </li>
                <li>
                  <span>Kecamatan</span>
                  <div class="row-fluid" id="text_kec">
                    <select class="form-control" style="width:100%; border-radius:0" id="select_kec" data-live-search="true" title="Data Provinsi">
                        
                    </select>
                  </div>
                </li>
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Data</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="kecamatan">
                            <label class="form-check-label" for="defaultCheck1">
                              MUI Kecamatan
                            </label>
                          </div>
                        </li>
                        <li>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="kabupaten">
                            <label class="form-check-label" for="defaultCheck2">
                              Mui Kabupaten
                            </label>
                          </div>
                        </li>
                        <li>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="provinsi">
                            <label class="form-check-label" for="defaultCheck3">
                              MUI Provinsi
                            </label>
                          </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Tempat Ibadah</a>
                    <ul class="collapse list-unstyled" id="pageSubmenu2">
                        <li>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="masjid">
                            <label class="form-check-label" for="defaultCheck1">
                              Masjid
                            </label>
                          </div>
                        </li>
                        <li>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="gereja">
                            <label class="form-check-label" for="defaultCheck2">
                              Gereja
                            </label>
                          </div>
                        </li>
                        <li>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="pura">
                            <label class="form-check-label" for="defaultCheck3">
                              Pura
                            </label>
                          </div>
                        </li>
                        <li>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="wihara">
                            <label class="form-check-label" for="defaultCheck3">
                              Wihara
                            </label>
                          </div>
                        </li>
                        <li>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="klenteng">
                            <label class="form-check-label" for="defaultCheck3">
                              Klenteng
                            </label>
                          </div>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="list-unstyled CTAs">
                <li>
                <?php if ($this->session->userdata('masuk') != TRUE) { ?>
                    <a href=<?= base_url().'auth' ?> class="download">Login</a>
                <?php } else { ?>
                    <a href="<?= base_url()."admin" ?>" class="download">
                        Dashboard
                    </a>

                <?php }?>


                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="padding:0; position:relative">

            <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin:20px; position:absolute; right: 0; left:0">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Maps</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Galery Halal</a>
                            </li>
                            <li class="nav-item" style="background: #cecece;">
                                <a class="nav-link" href="#">Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav> -->
            <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15812.016727728078!2d110.39641789999999!3d-7.789380049999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59db2dacb069%3A0x7f35156a4aeb645a!2sUIN+Sunan+Kalijaga+Yogyakarta!5e0!3m2!1sen!2sid!4v1541180179546" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe> -->
            <div id="map"></div>
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap-select.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCc54eSzWUQZ15kw8ITc_7VYk4qnURAWqs&callback=initMap">
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script type="text/javascript">
        window.base_url = <?php echo json_encode(base_url()); ?>;
    </script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/maps.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/select.js"></script>
    <!-- <script src="bootstrap-select.js"></script> -->
    <!-- <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });
    </script>  -->

</body>

</html>
