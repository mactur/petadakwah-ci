<section class="py-5">
  <div class="row">
    <div class="col-lg-12 mb-8">
      <div class="card">
        <div class="card-header">
          <h6 class="text-uppercase mb-0">Problem Keagamaan</h6>
        </div>
        <div class="card-body">
          <?php if ($this->session->userdata('akses') > 1) { ?>
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Kecamatan</label>
              <div class="col-md-5 select mb-3" id="kecamatan">
              </div>
            </div>
          <?php } if ($this->session->userdata('akses') > 2) { ?>
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Kabupaten</label>
              <div class="col-md-5 select mb-3" id="kabupaten">
              </div>
            </div>
          <?php } if ($this->session->userdata('akses') > 3) { ?>
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Provinsi</label>
              <div class="col-md-5 select mb-3" id="provinsi">
              </div>
            </div>
          <?php } ?>
              <a href="<?= base_url(); ?>admin/tproblem">
                <div class="input-group input-group-sm float-right" style="width:170px; margin-bottom:10px; padding-right:0">
                  <button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Data</button>
                </div>
              </a>
              <table id="data" class="table table-condensed dataTable no-footer" role="grid" aria-describedby="users-table_info">
                <thead>
                  <tr>
                    <th>#</th>


                    <th>Nama Problem</th>
                    <th>Tanggal</th>
                    <th>Lokasi Problem</th>
                    <?php if ($this->session->userdata('akses') >= 2) { ?>
                      <th>Kecamatan</th>
                    <?php } if ($this->session->userdata('akses') >= 3) { ?>
                      <th>Kabupaten</th>
                    <?php } if ($this->session->userdata('akses') >= 4) { ?>
                      <th>Provinsi</th>
                    <?php } ?>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i = 1;
                  foreach ($data as $dt) { ?>
                    <tr>
                      <td><?= $i ?></td>
                      <td><?= $dt['nama_problem'] ?></td>
                      <td><?= $dt['tanggal'] ?></td>
                      <td><?= $dt['lokasi_problem'] ?></td>
                      <?php if ($this->session->userdata('akses') >= 2) { ?>
                        <td><?= $dt['nama_kecamatan'] ?></td>
                      <?php } if ($this->session->userdata('akses') >= 3) { ?>
                        <td><?= $dt['kabupaten'] ?></td>
                      <?php } if ($this->session->userdata('akses') >= 4) { ?>
                        <td><?= $dt['provinsi'] ?></td>
                      <?php } ?>
                      <td>

                        <a href="<?= base_url().'admin/eproblem/'.$dt['id']?>" class="btn btn-warning btn-sm" title="Lihat atau Edit">
                          <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                        <a href="<?= base_url().'admin/p_hdata/'.$dt['id']?>">
                          <button class="btn btn-danger btn-sm" title="Hapus">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                          </button>
                        </a>
                      </td>
                    </tr>
                    <?php $i++;
                  } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
    </section>
