</div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">Your company &copy; 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by <a href="https://bootstrapious.com/admin-templates" class="external text-gray-400">Bootstrapious</a></p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->

    <script src="<?= base_url().'assets/' ?>vendor/js/bootstrap-select.js"></script>
    <script src="<?= base_url().'assets/' ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url().'assets/' ?>vendor/popper.js/umd/popper.min.js"> </script>
    <script src="<?= base_url().'assets/' ?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url().'assets/' ?>vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="<?= base_url().'assets/' ?>vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="<?= base_url().'assets/' ?>vendor/js/charts-home.js"></script>
    <script src="<?= base_url().'assets/' ?>vendor/js/front.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script> --}} -->
    <script src="<?= base_url().'assets/' ?>vendor/js/bootstrap-select.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#data').DataTable({
          initComplete: function () {
              this.api().columns([3]).every( function () {
                  var column = this;
                  var select = $('<select><option value=""></option></select>')
                      .appendTo($( "#tahunn" ))
                      .on( 'change', function () {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                          );

                          column
                              .search( val ? '^'+val+'$' : '', true, false )
                              .draw();
                      } );

                  column.data().unique().sort().each( function ( d, j ) {
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                  } );
              } );
              this.api().columns([4]).every( function () {
                  var column = this;
                  var select = $('<select><option value=""></option></select>')
                      .appendTo($( "#kecamatan" ))
                      .on( 'change', function () {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                          );

                          column
                              .search( val ? '^'+val+'$' : '', true, true )
                              .draw();
                      } );

                  column.data().unique().sort().each( function ( d, j ) {
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                  } );
              } );
              this.api().columns([5]).every( function () {
                  var column = this;
                  var select = $('<select><option value=""></option></select>')
                      .appendTo($( "#kabupaten" ))
                      .on( 'change', function () {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                          );

                          column
                              .search( val ? '^'+val+'$' : '', true, false )
                              .draw();
                      } );

                  column.data().unique().sort().each( function ( d, j ) {
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                  } );
              } );
              this.api().columns([6]).every( function () {
                  var column = this;
                  var select = $('<select><option value=""></option></select>')
                      .appendTo($( "#provinsi" ))
                      .on( 'change', function () {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                          );

                          column
                              .search( val ? '^'+val+'$' : '', true, false )
                              .draw();
                      } );

                  column.data().unique().sort().each( function ( d, j ) {
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                  } );
              } );
          }
          // processing: true,
          // serverSide: true,
          // ajax: 'https://datatables.yajrabox.com/eloquent/row-num-data',
          // columns: [
          //     // or just disable search since it's not really searchable. just add searchable:false
          //     {data: 'rownum', name: 'rownum'},
          //     {data: 'id', name: 'id'},
          //     {data: 'name', name: 'name'},
          //     {data: 'email', name: 'email'},
          //     {data: 'created_at', name: 'created_at'}
          //
          // ]
          // "order": [[ 0, "desc" ]]
        });
    });
    function edit(id) {
      window.location.href="/edata";
    }
    </script>
  </body>
</html>
