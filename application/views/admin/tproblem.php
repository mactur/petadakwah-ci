    <script type="text/javascript">
      var marker;
      var mapOptions;
      var map;
      window.onload = function () {
        var lats = document.getElementById('lats');
        var lngs = document.getElementById('lngs');
        mapOptions = {
          center: new google.maps.LatLng(-7.0378, 111.0611),
          zoom: 7,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var latlngbounds = new google.maps.LatLngBounds();
        map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        google.maps.event.addListener(map, 'click', function (e) {
          // alert("Latitude: " + () + "\r\nLongitude: " + e.latLng.lng());
          // var markers = new google.maps.Marker({
          //   position: e.latLng, 
          //   map: map
          // }); 
          lats.value = e.latLng.lat();
          lngs.value = e.latLng.lng();
          placeMarker(e.latLng);
        });
      }
      function placeMarker(location) {
        if ( marker ) {
          marker.setPosition(location);
        } else {
          marker = new google.maps.Marker({
            position: location,
            map: map
          });
        }
      }
    </script>
    <section class="py-5">
      <div class="row">
        <div class="col-lg-12 mb-5">
          <div class="card">
            <div class="card-header">
              <h3 class="h6 text-uppercase mb-0">Tambah Data Problem Keagamaan</h3>
            </div>
            <div class="card-body">

              <?php echo form_open('admin/p_tproblem'); ?>
              <input id="lats" type="hidden" name="lat" class="form-control">
              <input id="lngs" type="hidden" name="lng" class="form-control">
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Masalah</label>
                <div class="col-md-9">
                  <input type="text" placeholder="Nama Masalah" name="masalah" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Uraian Masalah</label>
                <div class="col-md-9">
                  <input type="text" placeholder="Uraian Masalah" name="uraian" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Lokasi Masalah</label>
                <div class="col-md-9">
                  <input type="text" placeholder="Lokasi Masalah" name="lokasi" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Tanggal Masalah</label>
                <div class="col-md-9">
                  <input type="date" placeholder="Tanggal Masalah" name="tanggal" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
               <div class="form-group row">
                <label class="col-md-3 form-control-label">Titik Lokasi ( Klik Peta )</label>
              </div>
              <div id="dvMap" style="width: 100%;height: 500px">
              </div> 
              <div class="line"></div>
              <br>
              <div class="form-group row">
                <div class="col-md-9 ml-auto">
                  <a href="<?= base_url(); ?>admin/problem">
                    <div type="submit" class="btn btn-secondary">Cancel</div>
                  </a>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
