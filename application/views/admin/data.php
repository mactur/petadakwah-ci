<section class="py-5">
  <div class="row">
    <div class="col-lg-12 mb-8">
      <div class="card">
        <div class="card-header">
          <h6 class="text-uppercase mb-0">Data Keagamaan <?= $this->session->userdata('akses') ?></h6>
        </div>
        <div class="card-body">
          <?php if ($this->session->userdata('akses') > 1) { ?>
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Kecamatan</label>
              <div class="col-md-5 select mb-3" id="kecamatan">
              </div>
            </div>
          <?php } if ($this->session->userdata('akses') > 2) { ?>
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Kabupaten</label>
              <div class="col-md-5 select mb-3" id="kabupaten">
              </div>
            </div>
          <?php } if ($this->session->userdata('akses') > 3) { ?>
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Provinsi</label>
              <div class="col-md-5 select mb-3" id="provinsi">
              </div>
            </div>
          <?php } ?>
          <div class="form-group row">
            <label class="col-md-2 form-control-label">Tahun</label>
            <div class="col-md-5 select mb-3" id="tahunn">
               <!-- <select id="years" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Pilih tahun.."> -->
                <?php
                  // $current_year = date('Y');
                  // $range = range($current_year, $current_year-10);
                  // $years = array_combine($range, $range);

                // foreach ($years as $y) { ?>
                  <!-- <option value="<?= $y ?>"><?= $y ?></option> -->
                <?php //} ?>
              <!-- </select> -->
            </div>
          </div>
          <a href="<?= base_url(); ?>admin/tdata">
          <div class="input-group input-group-sm float-right" style="width:170px; margin-bottom:10px; padding-right:0">
            <button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Data</button>
          </div>
        </a>
          <table id="data" class="table table-condensed dataTable no-footer" role="grid" aria-describedby="users-table_info">
            <thead>
              <tr>
                <th>#</th>
                <th>Kelurahan</th>
                <th>Jumlah Penduduk</th>
                <th>Tahun</th>
                <?php if ($this->session->userdata('akses') >= 2) { ?>
                  <th>Kecamatan</th>
                <?php } if ($this->session->userdata('akses') >= 3) { ?>
                  <th>Kabupaten</th>
                <?php } if ($this->session->userdata('akses') >= 4) { ?>
                  <th>Provinsi</th>
                <?php } ?>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $i = 1;
                foreach ($data as $dt) { ?>
                <tr>
                  <td><?= $i ?></td>
                  <td><?= $dt['nama_kelurahan'] ?></td>
                  <td><?= $dt['jumlah_penduduk'] ?></td>
                  <td><?= $dt['tahun'] ?></td>
                  <?php if ($this->session->userdata('akses') >= 2) { ?>
                    <td><?= $dt['nama_kecamatan'] ?></td>
                  <?php } if ($this->session->userdata('akses') >= 3) { ?>
                    <td><?= $dt['kabupaten'] ?></td>
                  <?php } if ($this->session->userdata('akses') >= 4) { ?>
                    <td><?= $dt['provinsi'] ?></td>
                  <?php } ?>
                  <td>

                    <a href="<?= base_url().'admin/edata/'.$dt['id_agama']?>" class="btn btn-warning btn-sm" title="Lihat atau Edit">
                      <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                     <a href="<?= base_url().'admin/p_hdata/'.$dt['id_agama']?>">
                    <button class="btn btn-danger btn-sm" title="Hapus">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                  </a>
                  </td>
                </tr>
                <?php $i++;
              } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
</section>
