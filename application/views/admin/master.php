<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Peta Dakwah MUI</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?= base_url().'assets/' ?>vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
     <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"> --}} -->
    <link rel="stylesheet" href="<?= base_url().'assets/' ?>vendor/css/bootstrap-select.css">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="<?= base_url().'assets/' ?>vendor/css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?= base_url().'assets/' ?>vendor/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?= base_url().'assets/' ?>vendor/css/custom.css">
    <!-- Favicon-->
       <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCc54eSzWUQZ15kw8ITc_7VYk4qnURAWqs&callback=initMap"></script>
    <link rel="shortcut icon" href="<?= base_url().'assets/' ?>vendor/img/favicon.png?3">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
    <style media="screen">
      .form-group {
        margin-bottom: 0
      }
    </style>
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a href="index.html" class="navbar-brand font-weight-bold text-uppercase text-base">Peta Dakwah</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">

          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><img src="<?= base_url().'assets/' ?>vendor/img/avatar-6.jpg" alt="Jason Doe" style="max-width: 2.5rem;" class="img-fluid rounded-circle shadow"></a>
            <div aria-labelledby="userInfo" class="dropdown-menu"><a href="#" class="dropdown-item"><strong class="d-block text-uppercase headings-font-family"><?= $this->session->userdata('nama') ?></strong><small><?= $this->session->userdata('ses_kdDaerah'); ?></small></a>
              <div class="dropdown-divider"></div>
              

               <a href="<?= base_url() ?>auth/logout" class="dropdown-item">
                  Logout
              </a>

            </div>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">
      <div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item"><a href="<?= base_url() ?>admin/data" class="sidebar-link text-muted"><i class="o-home-1 mr-3 text-gray"></i><span>Home</span></a></li>
              <li class="sidebar-list-item"><a href="<?= base_url() ?>admin/data" class="sidebar-link text-muted"><i class="o-sales-up-1 mr-3 text-gray"></i><span>Data Keagamaan</span></a></li>
              <li class="sidebar-list-item"><a href="<?= base_url() ?>admin/problem" class="sidebar-link text-muted"><i class="o-table-content-1 mr-3 text-gray"></i><span>Problem Keagamaan</span></a></li>
              <li class="sidebar-list-item"><a href="<?= base_url() ?>admin/organisasi" class="sidebar-link text-muted"><i class="o-survey-1 mr-3 text-gray"></i><span>Organisasi Keagamaan</span></a></li>
              <li class="sidebar-list-item"><a href="/halal" class="sidebar-link text-muted"><i class="o-survey-1 mr-3 text-gray"></i><span>Galery Halal</span></a></li>
        </ul>

      </div>
      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          
        