
  <section class="py-5">
    <div class="row">
      <div class="col-lg-12 mb-5">
        <div class="card">
          <div class="card-header">
            <h3 class="h6 text-uppercase mb-0">Edit Data Keagamaan</h3>
          </div>
          <div class="card-body">
            <?php echo form_open('admin/p_edata'); ?>
              <input type="hidden" name="id" value="<?= $keagamaan->id?>">
            <!--   <input type="hidden" name="_method" value="PUT"> -->
              
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Nama Kelurahan</label>
                <div class="col-md-9">
                  <input type="text" placeholder="Nama Kelurahan" value="<?= $keagamaan->nama_kelurahan?>" name="kelurahan" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Penduduk</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Penduduk" value="<?= $keagamaan->jumlah_penduduk?>" name="penduduk" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Islam</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Islam" value="<?= $keagamaan->islam?>" name="islam" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kristen Protestan</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah kristen protestan" value="<?= $keagamaan->kristen_protestan?>" name="protestan" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kristen katolik</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah kristen katolik" value="<?= $keagamaan->kristen_katolik?>" name="katolik" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah hindu</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah hindu" value="<?= $keagamaan->hindu?>" name="hindu" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah budha</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah budha" value="<?= $keagamaan->budha?>" name="budha" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah konghucu</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah konghucu" value="<?= $keagamaan->konghucu?>" name="konghucu" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah masjid</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah masjid" value="<?= $keagamaan->jml_masjid?>" name="masjid" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah pesantren</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah pesantren" value="<?= $keagamaan->pesantren?>" name="pesantren" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Dai</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Dai" value="<?= $keagamaan->jml_dai?>" name="dai" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Gereja</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Gereja" value="<?= $keagamaan->gereja?>" name="gereja" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Pura</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Pura" value="<?= $keagamaan->pura?>" name="pura" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kelenteng</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Kelenteng" value="<?= $keagamaan->kelenteng?>" name="kelenteng" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Keterangan</label>
                <div class="col-md-9">
                  <textarea name="keterangan" class="form-control"><?= $keagamaan->keterangan?></textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Tahun</label>
                <div class="col-md-9 select mb-3">
                 <input type="number" placeholder="YYYY" min="1900" max="2100" name="tahun"  class="form-control" value="<?= $keagamaan->tahun ?>">
                </div>
              </div>
              
              <div class="line"></div> 
              <div class="form-group row">
                <div class="col-md-9 ml-auto">
                  <a href="<?= base_url(); ?>admin/data">
                  <div type="reset" class="btn btn-secondary">Cancel</div>
                </a>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
           <?php echo form_close(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
